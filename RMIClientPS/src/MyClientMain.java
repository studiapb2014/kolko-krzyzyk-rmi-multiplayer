import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.RemoteObject;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Scanner;

public class MyClientMain extends RemoteObject {
    private static String IP = "192.168.137.1";

    public static void main(String[] args) {

        System.setProperty("java.security.policy", "security.policy");

        System.setSecurityManager(new SecurityManager());

        try {

            MyServerInt myRemoteObject = (MyServerInt) Naming.lookup("rmi://" + IP + "/ABC");

            Scanner scanner = new Scanner(System.in);
            System.out.println("1. Pobierz wszystkich");
            System.out.println("2. Pobierz po imieniu");
            RemoteClientImpl client = new RemoteClientImpl(myRemoteObject);
            myRemoteObject.connect(client);
            while (true) {
//                System.out.println("Set value X");
//                int x = new Scanner(System.in).nextInt();
//                System.out.println("Set value Y");
//                int y = new Scanner(System.in).nextInt();
//                myRemoteObject.setValue(x, y);
            }
        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}