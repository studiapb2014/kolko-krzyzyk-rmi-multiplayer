import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

public class RemoteClientImpl implements RemoteClientInterface, Serializable {
    private MyServerInt server;
    public RemoteClientImpl(MyServerInt serverInterface) throws RemoteException {
        UnicastRemoteObject.exportObject(this, 0);
        this.server = serverInterface;
    }
    @Override
    public void doSomething() throws RemoteException {
        System.out.println("Server invoked method");
    }

    @Override
    public void connectionSuccessful() throws RemoteException {
        System.out.println("Successful connected to server");
    }

    @Override
    public void refreshTable(int[][] gameArray) throws RemoteException {
        for(int a[] : gameArray){
            for(int b: a){
                System.out.print("|");
                System.out.print(b);
            }
            System.out.println("||");
        }
    }

    @Override
    public void getMessage(String message) throws RemoteException {
        System.out.println("Server :: " + message);
    }

    @Override
    public void setTurn() throws RemoteException, ServerNotActiveException {
        System.out.println("Set value X");
        int x = new Scanner(System.in).nextInt();
        System.out.println("Set value Y");
        int y = new Scanner(System.in).nextInt();
        server.setValue(x, y);
    }


}
