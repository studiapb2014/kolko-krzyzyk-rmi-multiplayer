import java.io.Serializable;

public class Customer implements  Serializable{
    private static final long serialVersionUID = 1L;
    private String Name;

    private String Surname;

    private String Address;

    public Customer() { }

    public Customer(String name, String surname, String address) {
        this.Name = name;
        this.Surname = surname;
        this.Address = address;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

}