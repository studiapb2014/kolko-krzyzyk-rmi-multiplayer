

import java.rmi.Remote;

import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.sql.SQLException;
import java.util.List;

public interface MyServerInt extends Remote {

	void connect(RemoteClientInterface client) throws RemoteException, ServerNotActiveException, InterruptedException;

	void setValue(int x, int y) throws RemoteException, ServerNotActiveException;
	int[][] showTable() throws RemoteException;

}