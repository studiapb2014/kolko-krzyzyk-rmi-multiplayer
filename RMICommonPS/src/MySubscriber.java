import java.io.Serializable;
import java.util.concurrent.Flow.Subscription;
import java.util.concurrent.Flow.Subscriber;

public class MySubscriber<T> implements Subscriber<T>, Serializable {
    private static final long serialVersionUID = 1L;
    private Subscription subscription;
    private String name;
    public MySubscriber(String name) {
        this.name = name;
    }

    @Override
    public void onComplete() {
        System.out.println(name + ": onComplete");
    }

    @Override
    public void onError(Throwable t) {
        System.out.println(name + ": onError");
        t.printStackTrace();
    }

    @Override
    public void onNext(T msg) {
        System.out.println(name + ": " + msg.toString() + " received in onNext");
        subscription.request(1);
    }

    @Override
    public void onSubscribe(Subscription subscription) {
        System.out.println(name + ": onSubscribe");
        this.subscription = subscription;
        subscription.request(1);
    }
}
