import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;

public interface RemoteClientInterface extends Remote, Serializable {
    void doSomething() throws RemoteException;
    void connectionSuccessful () throws  RemoteException;
    void refreshTable(int[][] gameArray) throws RemoteException;
    void getMessage(String message) throws  RemoteException;
    void setTurn() throws RemoteException, ServerNotActiveException;
}
