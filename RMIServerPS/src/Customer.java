import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.DatabaseTable;

import java.math.BigInteger;
import java.util.List;
import java.io.Serializable;
@DatabaseTable(tableName = "Customer")
public class Customer implements Serializable {
    private static final long serialVersionUID = 1L;
    @DatabaseField
    private String Name;

    @DatabaseField
    private String Surname;

    @DatabaseField
    private String Address;

    public Customer() { }

    public Customer(String name, String surname, String address) {
        this.Name = name;
        this.Surname = surname;
        this.Address = address;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

}