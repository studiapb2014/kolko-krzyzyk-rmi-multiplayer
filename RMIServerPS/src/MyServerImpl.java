

import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class MyServerImpl extends UnicastRemoteObject implements MyServerInt {

    private Map<String, RemoteClientInterface> clients;
    //	private Map<Integer,String> players;
    private ArrayList<String> players;
    private int gameArray[][];
    private int playerCounter = 0;
    private int round = 1;
    private int playerTurn = 0;
    private int winner = -1;
    CountDownLatch latch;

    protected MyServerImpl() throws RemoteException {
        super();
        gameArray = new int[3][3];
        clients = new HashMap<>();
//		players = new HashMap<>();
        players = new ArrayList<>();
        latch = new CountDownLatch(1);
    }

    @Override
    public void connect(RemoteClientInterface client) throws RemoteException, ServerNotActiveException, InterruptedException {

        serverLog("Player connected: {0}", getClientHost());
        clients.put(getClientHost(), client);
        players.add(getClientHost());
        client.connectionSuccessful();
        if (players.size() >= 2) {
        startGame();
        } else {
            waitForPlayers();
        }
    }

    @Override
    public void setValue(int x, int y) throws RemoteException, ServerNotActiveException {
        serverLog("IP :: {0}. Player :: {1}. Set value:: X: {2} Y:{3}",
                getClientHost(), playerTurn + 1, x, y);
        if (gameArray[x][y] != 0) {
            serverLog("Failed to set value, position is taken");
            notifyPlayers(MessageFormat.format("Sorry, positon X:{0} Y:{1} is already taken. Try again", x, y));
            latch.countDown();
            return;
        }
        gameArray[x][y] = playerTurn + 1;
        refreshTable();
        if (isWinner()) {
            winner = playerTurn;
            notifyPlayers("Player " + (playerTurn + 1) + " is winner");
        }
        setNextPlayer();
        latch.countDown();
    }

    private boolean isWinner() {
        for (int i = 0; i < 3; i++) {
            if (gameArray[0][i] == gameArray[1][i] && gameArray[1][i] == gameArray[2][i] && gameArray[2][i] != 0)
                return true;
            if (gameArray[i][0] == gameArray[i][1] && gameArray[i][1] == gameArray[i][2] && gameArray[i][2] != 0)
                return true;
        }
        if (gameArray[0][0] == gameArray[1][1] && gameArray[1][1] == gameArray[2][2] && gameArray[2][2] != 0)
            return true;
        if (gameArray[0][2] == gameArray[1][1] && gameArray[1][1] == gameArray[2][0] && gameArray[2][0] != 0)
            return true;
        return false;
    }

    private void setNextPlayer() throws RemoteException {
        if (players.size() - 1 > playerTurn) {
            ++playerTurn;
        } else {
            notifyPlayers("Round " + String.valueOf(++round));
            playerTurn = 0;
        }
    }

    private void notifyPlayers(String message) throws RemoteException {
        for (RemoteClientInterface client : clients.values()) {
            client.getMessage(message);
        }
    }

    private void refreshTable() throws RemoteException {
        for (RemoteClientInterface client : clients.values()) {
            client.refreshTable(gameArray);
        }
    }

    private void startGame() throws RemoteException, InterruptedException, ServerNotActiveException {
        notifyPlayers("Starting game...");
        while (winner == -1) {
            notifyPlayers("Player " + String.valueOf(playerTurn + 1) + " turn");
            getActualClient().setTurn();
            latch.await();
        }
    }

    private void waitForPlayers() throws RemoteException {
        notifyPlayers("Waiting for second player");
    }

    @Override
    public int[][] showTable() throws RemoteException {
        return gameArray;
    }

    private RemoteClientInterface getActualClient() {
        return clients.get(players.get(playerTurn));
    }

    private void serverLog(String message, Object... arguments) {
        System.out.println(MessageFormat.format(message, arguments));
    }

}