import javafx.beans.Observable;

import java.net.MalformedURLException;

import java.rmi.Naming;

import java.rmi.RemoteException;

import java.rmi.registry.LocateRegistry;
import java.sql.SQLException;
import java.util.concurrent.SubmissionPublisher;

public class MyServerMain {
    private static String serverIP = "192.168.137.1";

    public static void main(String[] args) throws SQLException {

        try {
            System.setProperty("java.security.policy", "security.policy");
            if (System.getSecurityManager() == null) {
                System.setSecurityManager(new SecurityManager());
            }
            System.setProperty("java.rmi.server.hostname", serverIP);
            System.setProperty("java.rmi.server.codebase", "file:/C:/Users/Mateusz/eclipse-workspace/PS_RMI/out/production/RMIServerPS/");
            System.out.println("Codebase: " + System.getProperty("java.rmi.server.codebase"));
            LocateRegistry.createRegistry(1099);
            MyServerImpl obj1 = new MyServerImpl();
            Naming.rebind("//" + serverIP + "/ABC", obj1);



//            SubmissionPublisher<String> publisher = new SubmissionPublisher<>();
//            MySubscriber<String> subscriber = new MySubscriber<>("Mine");
//            MySubscriber<String> subscriberYours = new MySubscriber<>("Yours");
//            publisher.subscribe(subscriber);
//            publisher.subscribe(subscriberYours);
//            publisher.submit("One");
//            publisher.submit("Two");
//            publisher.submit("Three");
//            publisher.submit("Four");
//            publisher.submit("Five");
//
//            publisher.close();

            System.out.println("Serwer oczekuje ...");

        } catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }

    }

}